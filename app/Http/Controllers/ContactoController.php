<?php

namespace App\Http\Controllers;

use App\Models\Contacto;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactoController extends Controller
{
    public function index()
    {
        $contactos = Contacto::all();
        return response()->json([
            'message' => "Show al contacts",
            "data" => $contactos
        ]);
    }

    public function store(Request $request)
    {
        $contacto = User::where('user', $request->contactoId)->first();
        if($contacto == null)
        {
            return response()->json([
                'message' => "El usuario ".$request->contactoId . " no existe!",
                'data' => null,
                'status' => false
            ]);
        }
        $validacion = Contacto::where('userId', $request->userId)->where('contactoId', $contacto->id)->first();
        if($validacion != null)
        {
            return response()->json([
                'message' => "El usuario ".$request->contactoId . " ya es tu contacto!",
                'data' => null,
                'status' => false
            ]);
        }
        $contact = new Contacto();
        $contact->userId = $request->userId;
        $contact->contactoId = $contacto->id;
        $contact->contactoName = $contacto->name;
        $contact->contactoUser = $contacto->user;
        $contact->save();

        return response()->json([
            'message' => "El usuario ".$request->contactoId . " es ahora tu contacto",
            'data' => $contact
        ]);
    }

    public function show($id)
    {
        $contact = Contacto::where('userId', $id)->get();
        return response()->json([
            'message' => "Contact show successfully",
            'data' => $contact
        ]);
    }

    public function destroy($id)
    {
        $contact = Contacto::find($id);
        $contact->delete();

        return response()->json([
            'message' => "Contact delete successfully",
            'data' => $contact
        ]);
    }
}
