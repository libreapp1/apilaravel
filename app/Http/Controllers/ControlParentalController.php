<?php

namespace App\Http\Controllers;

use App\Models\ControlParental;
use App\Models\User;
use Illuminate\Http\Request;

class ControlParentalController extends Controller
{
    public function store(Request $q)
    {
        $verificar = ControlParental::where('userId', $q->userId)->where('parentalUser', $q->usuario)->exists();
        if ($verificar) {
            return response()->json([
                "status" => false,
                "message" => "Ya tienes registrado a este usuario, si aun no te aprueba solicitale lo realize"
            ]);
        }

        $verificar = User::where('user', $q->usuario)->first();
        if ($verificar == null) {
            return response()->json([
                "status" => false,
                "message" => "Este usuario no existe en nuestra base de datos"
            ]);
        }

        $parental = new ControlParental();
        $parental->userId = $q->userId;
        $parental->parentalName = $verificar->name;
        $parental->parentalUser = $verificar->user;
        $parental->parentalId = $verificar->id;
        $parental->parentesco = $q->parentesco;
        $parental->status = $q->status;
        $parental->save();

        return response()->json([
            "status" => true,
            "data" => $parental,
            "message" => "Solicitud de control parental a: " . $verificar->name . " realizada con exito"
        ]);
    }

    public function show($control_parental)
    {
        return response()->json([
            "status" => true,
            "solicitud" => ControlParental::where('userId', $control_parental)->where('status', false)->get(),
            "control" => ControlParental::where('userId', $control_parental)->where('status', true)->get(),
            "message" => "obtenidos con exito"
        ]);
    }

    public function destroy(ControlParental $control_parental)
    {
        $control_parental->delete();

        return response()->json([
            "status" => true,
            "data" => $control_parental,
            "message" => "El control parental de: " . $control_parental->parentalName . " ha sido eliminado con exito"
        ]);
    }

    public function update(ControlParental $control_parental)
    {
        $control_parental->status = true;
        $control_parental->save();

        return response()->json([
            "status" => true,
            "data" => $control_parental,
            "message" => "El control parental de: " . $control_parental->parentalName . " ha sido aceptado con exito"
        ]);
    }

    public function presentadas($id)
    {
        $solicitudes = ControlParental::where('parentalId', $id)->where('status', false)->get();

        foreach ($solicitudes as $solicitud) {
            $solicitante = User::find($solicitud->userId);
            $solicitud->solicitante = $solicitante->name;
        }
        return response()->json([
            "count" => $solicitudes->count(),
            "data" => $solicitudes
        ]);
    }
}
