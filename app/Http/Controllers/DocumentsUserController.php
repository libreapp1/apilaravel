<?php

namespace App\Http\Controllers;

use App\Models\DocumentsUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class DocumentsUserController extends Controller
{
    public function register(Request $request)
    {
        // back
        $back = $result = $request->file('back')->storeOnCloudinary();
        $front = $result = $request->file('front')->storeOnCloudinary();

        $documents = new DocumentsUser();
        $documents->type = "REGISTRO";
        $documents->userId = $request->idUsuario;
        $documents->back = $back->getSecurePath();
        $documents->front = $front->getSecurePath();
        $documents->save();
       
        return response()->json([
            "message" => "Documents succefull",
            "data" => $documents
        ]);
    }
}
