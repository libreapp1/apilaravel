<?php

namespace App\Http\Controllers;

use App\Models\Emparejamiento;
use App\Models\User;
use Illuminate\Http\Request;

class EmparejamientoController extends Controller
{
    public function store(Request $request)
    {
        // validar si el usuario existe
        $pareja = User::where('user', $request->usuario)->first();
        if($pareja == null)
        {
            return response()->json([
                'message' => "El usuario ".$request->usuario . " no existe!",
                'data' => null,
                'status' => false
            ]);
        }

        $nuevo = new Emparejamiento();
        $nuevo->userId = $request->userId;
        $nuevo->userCitaId = $pareja->id;
        $nuevo->nombreCita = $pareja->name;
        $nuevo->userCita = $pareja->user;
        $nuevo->ubication = $request->ubication;
        $nuevo->save();

        return response()->json([
            'message' => "Tu cita con: ".$pareja->name . " ha sido registrada exitosamente!",
            'data' => null,
            'status' => true
        ]);
    }
}
