<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;


class FcmController extends Controller
{
    public function registerToken(Request $q)
    {
        $user = User::find($q->userId);
        $user->tokenFcm = $q->token;
        $user->save();

        return response(true);
    }
}
