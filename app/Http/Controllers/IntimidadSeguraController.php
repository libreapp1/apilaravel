<?php

namespace App\Http\Controllers;

use App\Models\IntimidadSegura;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Cloudinary\Cloudinary;
use Cloudinary\Api\Upload\UploadApi;
use Cloudinary\Configuration\Configuration;

class IntimidadSeguraController extends Controller
{
    public function store(Request $q)
    {
        if ($q->tipo == 'TOKEN') {
            $user = User::where('user', $q->usuario)->first();
            if ($user == null) {
                return response()->json([
                    "status" => false,
                    "message" => "Este usuario no existe en nuestra base de datos",
                    "data" => null
                ]);
            }

            if ($user->tokenFirma != $q->tokenFirma) {
                return response()->json([
                    "status" => false,
                    "message" => "El token del usuario no coincide",
                    "data" => null
                ]);
            }

            $intimidad = new IntimidadSegura();
            $intimidad->userId = $q->userId;
            $intimidad->intimidadUserId = $user->id;
            $intimidad->intimidadUser = $user->user;
            $intimidad->userName = $user->name;
            $intimidad->tipo = "TOKEN";
            $intimidad->firma = "Esta intimidad fue firmada por el token del usuario id: " . $user->id;
            $intimidad->save();

            return response()->json([
                "status" => true,
                "message" => "Intimidad Segura registrada con exito",
                "data" => $intimidad
            ]);
        } else {
            $documento = $q->file('front')->storeOnCloudinary();
            $intimidad = new IntimidadSegura();
            $intimidad->userId = $q->userId;
            $intimidad->intimidadUserId = $documento->getSecurePath();
            $intimidad->userName = $q->usuario;
            $intimidad->tipo = "DOCUMENTO";
            $intimidad->firma = "";
            $intimidad->save();

            return response()->json([
                "status" => true,
                "message" => "Intimidad Segura registrada con exito",
                "data" => $intimidad
            ]);
        }
    }

    public function addFirma(Request $q)
    {
        Configuration::instance([
            'cloud' => [
                "cloud_name" => "dpe20ljig",
                "api_key" => "699569721397695",
                "api_secret" => "rX-dQ62U5sqPkhwRZ90riBItvfE"
            ],
        ]);

        $intimidad = IntimidadSegura::find($q->intimidadId);
        $uploadApi = new UploadApi();
        $result = $uploadApi->upload($q->imagen);
        $intimidad->firma = $result['secure_url'];
        $intimidad->save();

        return response()->json([
            "status" => true,
            "message" => "Intimidad Segura registrada con exito",
            "data" => $intimidad
        ]);
    }
}
