<?php

namespace App\Http\Controllers;

use App\Models\Contacto;
use App\Models\User;
use App\Traits\PushTrait;
use Illuminate\Http\Request;
use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;

class MessagesController extends Controller
{

    use PushTrait;

    public function send(Request $q)
    {
        $user = User::find($q->userId);
        $to = substr($user->codeCountry, 1) . $user->phone;
        $message = $q->message; // Mensaje
        $curl = curl_init();

        $data = array(
            'to' => [$to],
            'from' => "SoyLibre",
            'message' => $message,
        );

        // Convierte el array de datos a formato JSON
        $jsonData = json_encode($data);

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://dashboard.360nrs.com/api/rest/sms',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $jsonData,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ' . base64_encode('mediosgcol:ZJuf75?$')
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function login($usuario, $mensaje)
    {
        $user = User::find($usuario);
        $to = substr($user->codeCountry, 1) . $user->phone;
        $message = $mensaje; // Mensaje
        $curl = curl_init();

        $data = array(
            'to' => [$to],
            'from' => "SoyLibre",
            'message' => $message,
        );
        $jsonData = json_encode($data);

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://dashboard.360nrs.com/api/rest/sms',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $jsonData,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ' . base64_encode('mediosgcol:ZJuf75?$')
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $this->sendPush($user->tokenFcm, $message);
        echo $response;
    }

    public function register(Request $q)
    {
        $user = User::find($q->userId);
        return $user;
    }

    public function panico(Request $q)
    {
        $message = "El usuario " . $q->name . " del app LIBRE activo el botón de " . $q->type . " " . $q->getSchemeAndHttpHost() . "/maps/" . $q->solicitudId;
        $contactos = Contacto::where('userId', $q->userId)->get();
        $curl = curl_init();
        $responses = [];
        foreach ($contactos as $contact) {
            $contacto = User::find($contact->contactoId);
            $to = substr($contacto->codeCountry, 1) . $contacto->phone; // Usar los datos del contacto

            $data = array(
                'to' => [$to],
                'from' => "SoyLibre",
                'message' => $message,
            );

            $jsonData = json_encode($data);

            // curl_setopt_array($curl, array(
            //     CURLOPT_URL => 'https://dashboard.360nrs.com/api/rest/sms',
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_CUSTOMREQUEST => 'POST',
            //     CURLOPT_POSTFIELDS => $jsonData,
            //     CURLOPT_HTTPHEADER => array(
            //         'Content-Type: application/json',
            //         'Authorization: Basic ' . base64_encode('mediosgcol:ZJuf75?$')
            //     ),
            // ));
            // $response = curl_exec($curl);
            // $responses[] = $response;
            if ($contacto->tokenFcm) {
                $this->sendPush($contacto->tokenFcm, $message);
            }
        }

        curl_close($curl);
        return response()->json([
            "data" => $responses,
            "message" => "exito",
            "status" => true
        ]);
    }

    public function emergencia(Request $q)
    {
        $message = "El usuario " . $q->name . " del app LIBRE solicito un/una " . $q->type . " " . $q->getSchemeAndHttpHost() . "/maps/" . $q->solicitudId;
        $contactos = Contacto::where('userId', $q->userId)->get();
        $curl = curl_init();
        $responses = [];
        foreach ($contactos as $contact) {
            $contacto = User::find($contact->contactoId);
            $to = substr($contacto->codeCountry, 1) . $contacto->phone; // Usar los datos del contacto

            $data = array(
                'to' => [$to],
                'from' => "SoyLibre",
                'message' => $message,
            );

            $jsonData = json_encode($data);

            // curl_setopt_array($curl, array(
            //     CURLOPT_URL => 'https://dashboard.360nrs.com/api/rest/sms',
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_CUSTOMREQUEST => 'POST',
            //     CURLOPT_POSTFIELDS => $jsonData,
            //     CURLOPT_HTTPHEADER => array(
            //         'Content-Type: application/json',
            //         'Authorization: Basic ' . base64_encode('mediosgcol:ZJuf75?$')
            //     ),
            // ));
            // $response = curl_exec($curl);
            // $responses[] = $response;
            if ($contacto->tokenFcm) {
                $this->sendPush($contacto->tokenFcm, $message);
            }
        }

        curl_close($curl);
        return response()->json([
            "data" => $responses,
            "message" => "exito",
            "status" => true
        ]);
    }


    public function call(Request $q)
    {
        $curl = curl_init();
        $to = "573045660107";
        $message = "Esta es una prueba de voz de la App Libre";  // Reemplaza con el mensaje que deseas enviar

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://dashboard.360nrs.com/api/rest/voice',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode(array(
                "to" => [$to],
                "message" => $message,
                "gender" => "F",
                "language" => "es_ES"
            )),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ' . base64_encode('mediosgcol:ZJuf75?$')
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        echo $response;
    }
}
