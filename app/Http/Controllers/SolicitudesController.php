<?php

namespace App\Http\Controllers;

use App\Models\Solicitudes;
use Illuminate\Http\Request;

class SolicitudesController extends Controller
{
    public function store(Request $q)
    {
        $solicitud = new Solicitudes();
        $solicitud->userId = $q->userId;
        $solicitud->type = $q->type;
        $solicitud->ejecutado = false;
        $solicitud->latitude = $q->latitude;
        $solicitud->longitude = $q->longitude;
        $solicitud->save();

        return response()->json([
            "status" => true,
            "message" => "Alerta de " . $q->type . " ha sido generada con exito",
            "data" => $solicitud
        ]);
    }

    public function maps(Solicitudes $solicitud)
    {
        $url = "https://www.google.com/maps/search/?api=1&query=" . $solicitud->latitude . "," . $solicitud->longitude . "&z=18&t=k";
        header("Location: " . $url);
    }
}
