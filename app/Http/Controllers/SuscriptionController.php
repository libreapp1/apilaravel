<?php

namespace App\Http\Controllers;

use App\Models\Suscription;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Checkout\Session;
use Stripe\Event;

class SuscriptionController extends Controller
{
    public function show($userId)
    {
        $suscription = Suscription::where('userId', $userId)->first();

        if($suscription == null)
        {
            return response()->json([
                "message" => "Usuario no cuenta con periodo trial o suscripcion",
                "status" => false
            ]);
        }

       $fechaFinCarbon = Carbon::createFromFormat('d-m-Y', $suscription->fechaFin);
        $fechaActual = Carbon::now();
        $activa = $fechaActual->lte($fechaFinCarbon);

        return response()->json([
            "status" => true,
            "message" => "Suscripcion creada con exito",
            "data" => $suscription,
            "sStatus" => $activa
        ]);

    }

    public function free($userId)
    {
        $fechaActual = Carbon::now();
        $fechaEnUnaSemana = $fechaActual->clone()->addDays(7);
        $fechaActualFormateada = $fechaActual->format('d-m-Y');
        $fechaEnUnaSemanaFormateada = $fechaEnUnaSemana->format('d-m-Y');

        $suscripcion = Suscription::where('userId', $userId)->first();

        if($suscripcion == null)
        {
            $suscripcion = new Suscription();
            $suscripcion->monto = 0;
            $suscripcion->fechaInicio = $fechaActualFormateada;
            $suscripcion->userId = $userId;
            $suscripcion->fechaFin = $fechaEnUnaSemanaFormateada;
            $suscripcion->save();
        }

        $fechaFinCarbon = Carbon::createFromFormat('d-m-Y', $suscripcion->fechaFin);
        $fechaActual = Carbon::now();
        $activa = $fechaActual->lte($fechaFinCarbon);

        return response()->json([
            "status" => true,
            "message" => "Suscripcion creada con exito",
            "data" => $suscripcion,
            "sStatus" => $activa
        ]);
    }

    public function paid($userId, Request $request)
    {
        Stripe::setApiKey('sk_test_51LfRWEKsS2yu5uQ4G4HTMMD3mTxSQYxGoWZgcaPd3IMRxatjnp1GNioHkU2ifLeoR6yn3g4HrdlQo7G9c8irfnDB002diJZkCl');

        // cliente
        $cliente = User::find($userId);

        $clienteExistente = \Stripe\Customer::all([
            'email' => $cliente->email,
            'limit' => 1,
        ]);

        if ($clienteExistente->total > 0) {
            $idCliente = $clienteExistente->data[0]->id;
        } else {
            // Crear un nuevo cliente en Stripe
            $nuevoCliente = \Stripe\Customer::create([
                'name' => $cliente->name,
                'email' => $cliente->email,
            ]);
            $idCliente = $nuevoCliente->id;
        }

        $session = Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price' => 'price_1ObXtCKsS2yu5uQ4jaVRkdkz',
                'quantity' => 1,
            ]],
            'mode' => 'subscription',
            'customer' => $idCliente,
            'client_reference_id' => $userId,
            'success_url' => $request->getSchemeAndHttpHost().'/suscripcion/',
            'cancel_url' => $request->getSchemeAndHttpHost().'/error',
        ]);

        return response()->json(['checkout_url' => $session->url]);
    }

    public function handleStripeWebhook(Request $request)
    {
        // Verificar la firma del webhook
        $payload = $request->getContent();
        $sigHeader = $request->header('Stripe-Signature');
        // grand-golden-vouch-loyal
        try {
            $event = Event::constructFrom(
                json_decode($payload, true),
                $sigHeader,
                'whsec_b98ad6d49d1b4eaf29b04972a27566b1f2014d252da2a1bfd91b2379fbd23953'
            );

            if ($event->type == 'checkout.session.completed') {
                $userId = $event->data->object->client_reference_id;

                $fechaActual = Carbon::now();
                $fechaEnUnaSemana = $fechaActual->clone()->addDays(30);
                $fechaActualFormateada = $fechaActual->format('d-m-Y');
                $fechaEnUnaSemanaFormateada = $fechaEnUnaSemana->format('d-m-Y');
                
                $suscripcion = Suscription::where('userId', $userId)->delete();

                $suscripcion = new Suscription();
                $suscripcion->monto = 1;
                $suscripcion->fechaInicio = $fechaActualFormateada;
                $suscripcion->userId = $userId;
                $suscripcion->fechaFin = $fechaEnUnaSemanaFormateada;
                $suscripcion->save();
            }

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function exito()
    {
        return view('exito');
    }
}
