<?php

namespace App\Http\Controllers;

use App\Models\Emparejamiento;
use App\Models\IntimidadSegura;
use App\Models\Solicitudes;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $user = User::where('user', $request->user)
            ->orWhere('email', $request->email)
            ->orWhere('phone', $request->phone)
            ->first();

        if ($user != null) {
            return response()->json([
                "status" => false,
                "message" => "Ya existe un usuario registrado con ese nombre de Usuario, numero de telefono o correo.",
            ]);
        }

        $user = new User();
        $user->user = $request->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->codeCountry = $request->codeCountry;
        $user->phone = $request->phone;
        $user->typeIdentity = $request->typeIdentity;
        $user->numberIdentity = $request->numberIdentity;
        $user->genre = $request->genre;
        $user->tokenFirma = $this->generateRandomNumbers();
        $user->save();


        return response()->json([
            "message" => 'User Create sucessfull',
            "token" => null,
            "data" => $user
        ]);
    }

    public function login(Request $request)
    {
        $user = User::where('phone', $request->phone)->where('codeCountry', $request->codeCountry)->first();

        if ($user == null) {
            return response()->json([
                "status" => false,
                "message" => "No existe un usuario registrado con esa informacion intente nuevamente!",
            ]);
        }

        return response()->json([
            "status" => true,
            "message" => 'Usuario obtenido con exito!',
            "data" => $user
        ]);
    }

    public function numeros(Request $q)
    {
        $emprarejar = Emparejamiento::where('userId', $q->userId)->count();
        $intimidad = IntimidadSegura::where('userId', $q->userId)->count();
        $solicitudes = Solicitudes::where('userId', $q->userId)->count();
        return response()->json([
            "citasCount" => $emprarejar + $intimidad,
            "alarmasCount" => $solicitudes
        ]);
    }


    public function generateRandomNumbers()
    {
        $randomNumbers = '';

        for ($i = 0; $i < 4; $i++) {
            $randomNumbers .= mt_rand(0, 9);
        }

        return $randomNumbers;
    }

    public function historial(Request $q)
    {
        $citas = Emparejamiento::where('userId', $q->userId)->get();
        $alarmas = Solicitudes::where('userId', $q->userId)->get();

        return response()->json([
            "status" => true,
            "citas" => $citas,
            "alarmas" => $alarmas
        ]);
    }

    public function profile(User $id)
    {
        return response()->json([
            "status" => true,
            "data" => $id,
            "message" => "Usuario obetnido"
        ]);
    }
}
