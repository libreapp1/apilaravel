<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class JwtMiddleware
{
   public function handle($request, Closure $next)
{
    try {
        JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
        return response()->json(['error' => 'Unauthorized', 'details' => $e->getMessage()], 401);
    }

    return $next($request);
}
}
