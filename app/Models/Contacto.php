<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use MongoDB\Laravel\Eloquent\Model;

// use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    use HasFactory;
    protected $connection = 'mongodb';

    protected $fillable = [
        'userId',
        'contactoId',
        'contactoName',
        'contactoUser'
    ];
    


}
