<?php

namespace App\Traits;

use sngrl\PhpFirebaseCloudMessaging\Client;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Notification;

trait PushTrait
{
    public function sendPush($token, $mensaje)
    {
        $server_key = 'AAAAyn9X324:APA91bHxKGCkcvUSQWB9IjL-q4hwy68C2NFiHJuCZn1d0ufR1bTpLHQtxyXBrKz7MKKE8YAYSy7PUqoLvO-mNUAri4IjRJNcm_ka2jaa7aKu-PJeNopftYtbJTdn2GvlogWqq62SZJx7';
        $client = new Client();
        $client->setApiKey($server_key);
        $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->setPriority('high');
        $message->addRecipient(new Device($token));
        $message
            ->setNotification(new Notification('App Libre', $mensaje))
            ->setData(['key' => 'value']);

        $response = $client->send($message);

        return $response;
    }
}
