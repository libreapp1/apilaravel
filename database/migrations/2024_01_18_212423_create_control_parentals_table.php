<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('control_parentals', function (Blueprint $table) {
            $table->id();
            $table->string('userId');
            $table->string('userName');
            $table->string('userUser');
            $table->string('parentalId');
            $table->string('parentalName');
            $table->string('parentalUser');
            $table->string('parentesco');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('control_parentals');
    }
};
