<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('emparejamientos', function (Blueprint $table) {
            $table->id();
            $table->string('userId');
            $table->string('userName');
            $table->string('userUser');
            $table->string('citaId');
            $table->string('citaName');
            $table->string('citaUser');
            $table->string('ubicacion');
            $table->date('fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('emparejamientos');
    }
};
