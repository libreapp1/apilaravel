<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('intimidad_seguras', function (Blueprint $table) {
            $table->id();
            $table->string('userId');
            $table->string('userName');
            $table->string('userUser');
            $table->string('intimidadId');
            $table->string('intimidadName');
            $table->string('intimidadUser');
            $table->string('type');
            $table->text('documento')->nullable();
            $table->text('firma');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('intimidad_seguras');
    }
};
