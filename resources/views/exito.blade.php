<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Título de tu Página</title>
  <style>
    body {
      background: #6f3ed8;
      display: flex;
      align-items: center;
      justify-content: center;
      height: 100vh;
      margin: 0;
      flex-direction: column;
    }

    img {
      max-width: 100%;
      max-height: 100%;
      height: auto;
      width: auto;
    }

    p {
      margin-top: 10px;
    }

    a {
      color: white;
      text-decoration: none;
      font-weight: bold;
      margin-top: 20px; /* Ajusta según sea necesario */
    }
  </style>
</head>
<body>
  <img src="/images/logo.svg" alt="Logo"> 
  <p>Suscripcion Exitosa</p>
  <a href="libre_ionic://home">Volver a la aplicación</a>
</body>
</html>
