<?php

use App\Http\Controllers\ContactoController;
use App\Http\Controllers\ControlParentalController;
use App\Http\Controllers\DocumentsUserController;
use App\Http\Controllers\EmparejamientoController;
use App\Http\Controllers\IntimidadSeguraController;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\SolicitudesController;
use App\Http\Controllers\SuscriptionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FcmController;
use Illuminate\Support\Facades\Route;


Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);
Route::post('numeros', [UserController::class, 'numeros']);
Route::post('historial', [UserController::class, 'historial']);
Route::get('profile/{id}', [UserController::class, 'profile']);

Route::get('obtenerSusbcripcion/{userId}', [SuscriptionController::class, 'show']);
Route::get('freeSusbcripcion/{userId}', [SuscriptionController::class, 'free']);
Route::get('paidSusbcripcion/{userId}', [SuscriptionController::class, 'paid']);
Route::post('stripe-webhooks', [SuscriptionController::class, 'handleStripeWebhook']);

Route::resource('contacts', ContactoController::class);
Route::resource('emparejar', EmparejamientoController::class);
Route::resource('documents', DocumentsUserController::class);
Route::post('documents', [DocumentsUserController::class, 'register']);
Route::resource('intimidad-segura', IntimidadSeguraController::class);
Route::post('intimidad-segura/addFirma', [IntimidadSeguraController::class, 'addFirma']);
Route::resource('solicitudes', SolicitudesController::class);
Route::post('/sms/send', [MessagesController::class, 'send']);

Route::post('alertas/panico', [MessagesController::class, 'panico']);
Route::post('alertas/emergencias', [MessagesController::class, 'emergencias']);
Route::get('/call', [MessagesController::class, 'call']);
Route::resource('control-parental', ControlParentalController::class);
Route::get('control-parental/presentadas/{id}', [ControlParentalController::class, 'presentadas']);
Route::get('control-parental/update/{control_parental}', [ControlParentalController::class, 'update']);
Route::get('/sms/login/{usuario}/{mensaje}', [MessagesController::class, 'login']);


//fcm
Route::post('fcm/registerToken', [FcmController::class, 'registerToken']);
