<?php

use App\Http\Controllers\SolicitudesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SuscriptionController;


Route::get('/', function () {
    return view('welcome');
});


Route::get('suscripcion', [SuscriptionController::class, 'exito']);
Route::get('maps/{solicitud}', [SolicitudesController::class, 'maps']);
